# Geminispace de Picasoft

Gemini est un protocole plus léger que le web et qui respecte la vie privée des utilisateurs. Il permet de partager de l'inforamtion tout comme le web ou gopher.

Picasoft possède une capsule ainsi qu'un service de miroir Mastodon.

Gemserv fait office de reverse proxy et de serveur pour des fichiers statiques, il permet de déclarer plusieurs *virtual hosts*. La capsule est par exemple servie par celui-ci et le contenu de la capsule est sur un volume monté sur `/var/gemini`.

MastoGem est un proxy Mastodon pour Gemini, ici connecté à `mamot.fr`.

## Premier lancement

L'utilisation de TLS est obligatoire avec Gemini, on préfère utiliser des certificats signés par nous-même. Avant de lancer le service, il faut générer une clé (on ne la chiffre pas, les images supportent pas encore les clé chiffrées) :

```
openssl genrsa -out key.rsa 4096
```

Puis on génère deux certificats (un pour chaque service) en pensant à bien indiquer le domaine pour lequel le certificat est signé dans le sujet :

```
openssl req -x509 -key key.rsa -out cert.pem -days 365 -subj "/CN=picasoft.net"
openssl req -x509 -key key.rsa -out cert-toot.pem -days 365 -subj "/CN=mastogem.picasoft.net"
```

Ces certificats ainsi que la clé doivent être placés dans le dossier `certs`.

## Renouveller les certificats

On garde une durée de validité d'un an pour les certificats, ensuite il faut les renouveller (pas la clé) grâce aux même commandes que précédemment :

```
openssl req -x509 -key key.rsa -out cert.pem -days 365 -subj "/CN=picasoft.net"
openssl req -x509 -key key.rsa -out cert-toot.pem -days 365 -subj "/CN=mastogem.picasoft.net"
```
